**Coursework 2**

**Medbot**

Medbot is a chatbot written in C. The base algorithm for this chatbot is based on keywords. The chatbot will search for the keyword in user input and output a suitable response to the keyword. To get diagnosed by the bot, simply input one symptom at a time. 
Example :- 
1. I feel very tired
2. I have fever
3. I coughed a lot lately / Excessive mucus is in my nose
4. I can't breathe properly/ I have breathing difficulty 
5. My muscle aches/ My body is in pain
6. I experience hair loss /I lost some weights

_This chatbot is only for informational purposes and is not qualified as an expert opinion. The diagnosis by this chatbot is not accurate and not suitable to be use during emergencies._


**URL of Git Repository**

https://gitlab.com/comp1921_2021/sc20sbz


**Compilation on LINUX**

1. Open Terminal
2. Input cd CWK2
3. Input mkdir build
4. Input cd build
5. Compile the program with cmake .. 
6. Then compile the program with make
7. Execute the program with ./chatbot
8. Answer the first question from medbot with either the word "Female" or "Male" only
9. Start having conversation with the bot - Input ONLY ONE medical symptom at a time
10. To quit the conversation, type "quit"
11. To run the Unity unit tests, just type ./unit_tests



**List of Keywords**

- Greetings : Hello, Hey
- Advice : Advice, Advise, What should i do
- Exercise : Exercise, Exercises
- Medicine : Medicine, Pills, Vitamins
- Appointment : Appointment, Doctor, Book appointment
- Covid-19: Covid-19, Covid, Coronavirus
- Ear Infection : Ears, Ear, Pain inside ear
- Breathing Difficulties : Accelerated breathing, Breathing, Short of breath,
- Influenza : Cough, Mucus, Fever, Flu, Sneeze, Sore throat, Running nose
- Fever : Body sore, Shivering,Vomit, Flu, Lost my appetite
- Cancer : Lump, Bleeding, Jaundice, Fatigue, Seizure, Lost weight
- Feelings : Depressed, Upset, Sad, Tired, Unhappy, Anxiety
- Body Parts : Chest, Head, Mouth, Heart
- Allergy : Rashes, Swollen, Itchy, Redness



**Git Commit History**
Git commit History for this project can be found in githistory.txt


**Extra**

- The volume of the video that demonstrate the functionality of the Medbot is low, please use earphones for better experience. 

                        
