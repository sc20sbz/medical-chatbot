#include "interface.h"
#include "knowledge.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void run_interface() {
    printf("\t  MEDBOT \n");
	printf("I am MEDBOT, the self symptom checker bot.\n\n");
	char input[1000];
	printf("To get started, please state your sex - male or female?\n\n");
	do{
        write_chat(input);
		printf("user : ");
        fgets(input, sizeof(input), stdin);
        trim_trailing(input);
        //printf("lepas trim : %s", input);
		convert_lowercase(input);
		//printf("lepas lower case : %s", input);
		//remove_punc(input);
		//printf("lepas remove punc : %s", input);
		trim_leading(input);
		//printf("lepas trim leading: %s", input);
		putchar('\n');

        if(strcmp(input, "quit\n")==false)
            end();
        else if(input == NULL || input[0]=='\n')
            printf("Medbot : Please input one symptom that you have\n");
        else if(match_reply(intelligence_keyword, input)==true)
			replace_substring(input);
        else if(match_reply(start_keyword, input)==true)
			print_start();
        else if(match_reply(gender_keyword, input)==true)
			print_gender();
		else if(match_reply(covid_keyword,input)==true)
			print_covid();
        else if(match_reply(feel_keyword,input)==true)
			print_feel();
        else if(match_reply(cancer_keyword,input)==true)
			print_cancer();
        else if(match_reply(influenza_keyword,input)==true)
			print_influenza();
        else if(match_reply(breathing_keyword,input)==true)
			print_breathing();
        else if(match_reply(ear_keyword,input)==true)
			print_ear();
        else if(match_reply(fever_keyword,input)==true)
			print_fever();
        else if(match_reply(advice_keyword,input)==true)
			print_advice();
        else if(match_reply(vit_keyword,input)==true)
			print_vit();
        else if(match_reply(exercise_keyword,input)==true)
			print_exercise();
        else if(match_reply(apt_keyword,input)==true)
			print_apt();
		else if(match_reply(body_keyword,input)==true)
			print_body();
		else if(match_reply(yes_keyword, input)==true)
			print_yes();
		else if(match_reply(med_keyword,input)==true)
			print_med();
		else if(match_reply(allergy_keyword,input)==true)
			print_allergy();
		else if(match_reply(thank_keyword,input)==true)
			print_thank();
		else if(match_reply(bot_keyword,input)==true)
			print_bot();
		else
			print_casual();
        search_file(input);
		putchar('\n');

	} while(true);
}




