#include "knowledge.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

void end(){
	printf("Medbot : Goodbye, I am glad to be of help\n");
	exit(1);
	
}

int random_reply(int x){
	int y=rand()% x;
	return y ;
}

char *casual_reply[]={
	"Sorry, I don't know about this",
	"Can you elaborate on that?",
	"I would be thankful if you can proceed.",
	"You're being a bit brief, perhaps you could go into detail.",
	"Can you be more detailed in explaining?",
	"Please tell me more",
	"Please continue",
	"Okay. Thank you for letting me know. What else?",
	"I could not understand what you are saying",
	"I am listening.. ",
};


void print_casual(){
	printf("Medbot : %s\n",casual_reply [random_reply(9)]);
}


char *bot_keyword[]={
	"you",
};

char *bot_reply[]={
	"We are talking about you, not me. Input one symptom at a time",
};

void print_bot(){
	printf("Medbot : %s\n",bot_reply[0]);
}

char *intelligence_keyword[]={
	"i am",

};

char *covid_keyword[]={
	"covid",
	"coronavirus",
	"covid19"
};

char *covid_reply[]={
	"Sorry, as of now, I don't have much information on covid",
};

void print_covid(){
	printf("Medbot : %s\n",covid_reply[0]);
}

char *start_keyword[]={
	"hello",
	"hey",
};

char *start_reply[]={
	"Welcome to self-Diagnosis Medical Chat-Bot",
};

void print_start(){
	printf("Medbot : %s\n",start_reply[0]);
}


char *yes_keyword[]={
	"yes",
	"no",

};

char *yes_reply[]={
	"Noted. What else?",
};

void print_yes(){
	printf("Medbot : %s\n",yes_reply[0]);
}


char *gender_keyword[]={
	"female",
	"male",
};

char *gender_reply[]={
	"Thank you for answering. Now, please input your symptom one at a time",
};

void print_gender(){
	printf("Medbot : %s\n",gender_reply[0]);
}


char *advice_keyword[]={
	"advice",
	"advise",
	"what should i do",
	"help",
	"blood pressure",
};

char *advice_reply[]={
	"Please do regular check up",
	"Book an appointment with your GP for more accurate results",
	"Please do regular exercise and follow a healthy diet",
	"Make sure that you drink enough water daily",
	"Take some vitamins to improve your health",
};

void print_advice(){
	printf("Medbot : %s\n",advice_reply[random_reply(5)]);
}


char *vit_keyword[]={
	"vitamins",
	"vitamin",
};

char *vit_reply[]={
	"Vitamins are organic compounds that are essential for human body. You can get them through healthy diet",
};

void print_vit(){
	printf("Medbot : %s\n",vit_reply[0]);
}

char *med_keyword[]={
	"medication",
	"medications",
	"medicine",
	"medicines",
	"pill",
	"pills",
};

char *med_reply[]={
	"Sorry, I am not licensed to inform or prescribe you on medications",
};

void print_med(){
	printf("Medbot : %s\n",med_reply[0]);
}

char *exercise_keyword[]={
	"what exercises",
	"what exercise",
	"exercise",
	"exercises",
};

char *exercise_reply[]={
	"Exercises can be easily done at home. Some examples include walking, squats or sit up",
};

void print_exercise(){
	printf("Medbot : %s\n",exercise_reply[0]);
}

char *thank_keyword[]={
	"thanks",
	"thank you",
	"bye",
};

char *thank_reply[]={
	"Okay. Happy to help!",
};

void print_thank(){
	printf("Medbot : %s\n",thank_reply[0]);
}


char *apt_keyword[]={
    "appointment",
	"book appointment",
	"doctor",
	"heart attack",
	"complication",
	"numb",
	"suicide",
	"insomnia",
	"see",
};

char *apt_reply[]={
	"Please contact your GP and make telephone appointment",
};

void print_apt(){
	printf("Medbot : %s\n",apt_reply[0]);
}


char *ear_keyword[]={
	"ears",
	"ear",
	"inside ear",
	"hear",
};

char *ear_reply[]={
	"The symptom that you have is related to ear infections. Ear infection usually occurs when bacteria affects the middle ear",
};

void print_ear(){
	printf("Medbot : %s\n",ear_reply[random_reply(1)]);
}


char *breathing_keyword[]={
	"accelerated breathing",
	"breathing",
	"breathe",
	"breathing fast",
	"breathing difficulty",
	"short of breath",
};

char *breathing_reply[]={
	"The possible medical cause for the symptom are anxiety, asthma or heart failure",
};

void print_breathing(){
	printf("Medbot : %s\n",breathing_reply[random_reply(1)]);
}

char *influenza_keyword[]={
	"cough",
	"mucus",
	"throat",
	"sneeze",
	"sneezing",
	"coughing",
	"running nose",
	"runny nose",
	"fever",
	"thirsty",
};

char *influenza_reply[]={
	"I think you should check if you have influenza or better known as flu",
	"You might have caught a cold. Please get enough rest if you do not feel pain",
	"Okay. I am glad that you reached out. You have the symptom for sinus",
	"Your symptom is closely linked to flu. For most people, the flu can resolve on its own",
};

void print_influenza(){
	printf("Medbot : %s\n",influenza_reply[random_reply(3)]);
}


char *cancer_keyword[]={
	"bleeding",
	"lump",
	"jaundice",
	"seizure",
	"weight",
	"fatigue",
	"swelling",
	"hair loss",
	"lost weight",
};

char *cancer_reply[]={
	"You have the general symptom of cancer.To confirm the diagnosis, please contact your GP immediately",
	"If you have pain that is severe and have other symptoms of cancer, you should see a doctor",
};

void print_cancer(){
	printf("Medbot : %s\n",cancer_reply[random_reply(2)]);
}

char *feel_keyword[]={
	"depressed",
	"upset",
	"lonely",
	"unhappy",
	"worried",
	"exhausted",
	"angry",
	"sick",
	"tired",
	"nervous",
	"anxiety",
	"pain",
	"bored",
	"sleep",
};

char *feel_reply[]={
	"What cause you feel like that?",
	"Are you feeling that often?",
	"Do you know what might causes you to feel that?",
	"How long have you been feeling it?"
};


void print_feel(){
    printf("Medbot : %s\n",feel_reply[random_reply(4)]);
}


char *body_keyword[]={
	"chest",
	"head",
	"heart",
	"hands",
	"legs",
	"hand",
	"leg",
	"mouth",
	"eyes",
	"eye",
	"finger",
	"fingers",
	"arm",
	"arms",
	"tongue",
	"knees",
	"ankle",
	"nose",
	"taste",
};

char *body_reply[]={
	"Can i know if you are sick often?",
	"I need a little more detail please.",
	"Could it be because you have not been taking your medicine?",
};

void print_body(){
	printf("Medbot : %s\n",body_reply[random_reply(3)]);

}

char *allergy_keyword[]={
	"rash",
	"rashes",
	"itchy",
	"itchy nose",
	"stomachache",
	"eye irritation",
	"irritation",
	"redness",
	"red",
	"smell",
	"swollen",
	"watering eyes",
};

char *allergy_reply[]={
	"You have allergy symptom. Please consult doctor to know your allergy",
};

void print_allergy(){
	printf("Medbot : %s\n",allergy_reply[0]);
}

char *fever_keyword[]={
	"body",
	"sore",
	"diarrhea",
	"lost my appetite",
	"muscle aches",
	"muscle ache",
	"aches",
	"ache",
	"vomit",
	"vomited",
	"vomiting",
	"shivering",
	"shivers",
	"shiver"
	"flu",
	"headache",
	"nauseated",
	"nausea",
	"chills",
};

char *fever_reply[]={
	"Depending on what's causing you to feel that, you are having the sign of fever",
	"If you feel hot and sweaty too, you might have a fever",
};

void print_fever(){
	printf("Medbot : %s\n",fever_reply[random_reply(2)]);
}



char * convert_lowercase(char *str){
	char *result = str;
	char *v;
	for(v = str; *v; v++){
        *v = tolower(*v);
    }
    return result;
}


char * remove_punc(char *str)
{
    char *result = str;
    char *v;
    for(v = str; *v; v++){
    	if (isalpha(*v)!=0){
      		*str++ = *v;
      	}
    }
    *str = '\0';
    return result;
}


char * trim_trailing(char *str)
{
    char *result =str;
    int x;
    int y = -1;
	for(x=0;str[x]!= '\0'; x++){
	  if(str[x]!=' '&& str[x]!='\t'){
	  	y=x;
	  }
	}
    str[y+1] = '\0';
    return result;
}

char * trim_leading(char * str)
{
   char * result = str;
   int x,y;
	for(x=0;str[x]==' '||str[x]=='\t';x++);	
		for(y=0;str[x];x++,y++){
		str[y]=str[x];
	}
	str[y]='\0'; 

    return str;
}


bool search_key(char *word,char *str){
    char temp[100];
	bool result;
	result =0;
    temp[strlen(temp)-1]='\0';
    if (strstr(str,word))
    {
        result = true;
    }
    return result;
}


bool match_reply(char **reply,char *str){
	bool result;
	for(result=0;*reply!=NULL;reply++)
		if(search_key(*reply,str)==true){
			result = true;
			break;
		}
	return result;
}


int write_chat(char *str){
    FILE *file;
    file = fopen("chat.txt", "w+");
    if (file == NULL)
    {
        printf("File could not be opened\n");
        return -1;
    }
    fprintf(file, "%s\n", str);
    fclose(file);
}


int search_file(char *str) {
	int x = 1;
	int result = 0;
	char temp[1000];
	
	FILE *fp;
    fp = fopen("chat.txt", "rt");
	if(fp == NULL) {
		return -1;
	}
	while(fgets(temp, 1000, fp) != NULL) {
		if((strstr(temp, str)) != NULL && strcmp(str, "") && str[0]!='\n') {
			printf("\n****Please dont repeat yourself next time***\n");
			result++;
		}
		x++;
	}
    fclose(fp);
}


void replace_substring(char *str)
{
    char temp[1000];
    char replace[100] ="i am";
    char newstr[100] = "so you are telling me you are";
    if((str = strstr(str, replace))==false)
        strncpy(temp, str, str-str);
    printf( "%s%s", newstr, str+strlen(replace));

}
