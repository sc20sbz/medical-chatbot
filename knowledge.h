#ifndef KNOWLEDGE_GUARD__H
#define KNOWLEDGE_GUARD__H
#include <stdbool.h>
#include <ctype.h>

//function that is being called to end the chatbot program
void end();

//get random reply that exists in any suitable reply arrays
int random_reply(int n);

//stores replies that should be printed out
//when the user talks about topics outside of the chatbot knowlegde
extern char *casual_reply[];

//print out the reply exist in casual_reply[] randomly
void print_casual();

//stores keyword that user might use to talk about medbot
extern char *bot_keyword[];

//stores replies that should be printed out
//when the user talks about medbot
extern char *bot_reply[];

//print out the reply exist in bot_reply[] 
void print_bot();

//stores keyword that user might use when thanking the medbot
extern char *thank_keyword[];

//stores replies that should be printed out
//when the user says thank you
extern char *thank_reply[];

//print out the reply exist in bot_thank[] 
void print_thank();


//stores keyword "I am" to show the intelligence
//of the chatbot
extern char *intelligence_keyword[];

//stores keyword that user might use
//to answer the chatbot
extern char *yes_keyword[];

//stores replies that should be printed out
//when the user answer the chatbot with any keyword from yes_keyword[]
extern char *yes_reply[];

//print out the reply exists in yes_reply[]
void print_yes();

//stores keyword that users might use when talk about gender
extern char *gender_keyword[];

//stores replies that should be printed out
//when the user input any keyword from gender_keyword[]
extern char *gender_reply[];

//print out the reply exists in gender_reply[]
void print_gender();

//stores keyword that users might use when talk about vitamins
extern char *vit_keyword[];

//stores replies that should be printed out
//when the user input any keyword from vit_keyword[]
extern char *vit_reply[];

//print out the reply exists in vit_reply[]
void print_vit();

//stores keyword that users might use when talk about medicines
extern char *med_keyword[];

//stores replies that should be printed out
//when the user input any keyword from med_keyword[]
extern char *med_reply[];

//print out the reply exists in med_reply[]
void print_med();

//stores keyword that users might use when talk about exercise
extern char *exercise_keyword[];

//stores replies that should be printed out
//when the user input any keyword from exercise_keyword[]
extern char *exercise_reply[];

//print out the reply exists in exercise_reply[]
void print_exercise();

//stores keyword that users might use when talk about appointment or doctor
extern char *apt_keyword[];

//stores replies that should be printed out
//when the user input any keyword from apt_keyword[]
extern char *apt_reply[];

//print out the reply exists in apt_reply[]
void print_apt();

//stores keyword that users might use when talk about breathing difficulties
extern char *breathing_keyword[];

//stores replies that should be printed out
//when the user input any keyword from breathing_keyword[]
extern char *breathing_reply[];

//print out the reply exists in breathing_reply[]
void print_breathing();

//stores keyword that users might use to start conversation
//with the chatbot like hey, hello or start
extern char *start_keyword[];

//stores replies that should be printed out
//when the user input any keyword in start_keyword[]
extern char *start_reply[];

//print out the reply exists in start_reply[]
void print_start();

//stores keyword that users might use when talk about covid symptoms
extern char *covid_keyword[];

//stores replies that should be printed out
//when the user input any keyword in covid_keyword[]
extern char *covid_reply[];

//print out the reply exists in covid_reply[] randomly
void print_covid();

//stores keyword that users might use when asking for advice
extern char *advice_keyword[];

//stores replies that should be printed out
//when the user input any keyword in advice_keyword[]
extern char *advice_reply[];

//print out the reply exists in advice_reply[] randomly
void print_advice();

//stores keyword that users might use when talk about ear infections
extern char *ear_keyword[];

//stores replies that should be printed out
//when the user input any keyword in ear_keyword[]
extern char *ear_reply[];

//print out the reply exists in ear_reply[] randomly
void print_ear();

//stores keyword that users might use when talk about influenza symptoms
extern char *influenza_keyword[];

//stores replies that should be printed out
//when the user input any keyword in influenza_keyword[]
extern char *influenza_reply[];

//print out the reply exists in influenza_reply[] randomly
void print_influenza();

//stores keyword that users might use when talk about cancer
extern char *cancer_keyword[];

//stores replies that should be printed out
//when the user input any keyword in cancer_keyword[]
extern char *cancer_reply[];

//print out the reply exists in cancer_reply[] randomly
void print_cancer();

//stores keyword that users might use when talk about what they feel emotionally
extern char *feel_keyword[];

//stores replies that should be printed out
//when the user input any keyword in feel_keyword[]
extern char *feel_reply[];

//print out the reply exists in feel_reply[] randomly
void print_feel();

//stores keyword that users might use when talk about body parts
extern char *body_keyword[];

//stores replies that should be printed out
//when the user input any keyword in body_keyword[]
extern char *body_reply[];

//print out the reply exists in body_reply[] randomly
void print_body();

//stores keyword that users might use when talk about fever symptoms
extern char *fever_keyword[];

//stores replies that should be printed out
//when the user input any keyword in fever_keyword[]
extern char *fever_reply[] ;

//print out the reply exists in fever_reply[] randomly
void print_fever();

//stores keyword that users might use when talk about allergy symptoms
extern char *allergy_keyword[];

//stores replies that should be printed out
//when the user input any keyword in allergy_keyword[]
extern char *allergy_reply[];

//print out the reply exists in allergy_reply[] randomly
void print_allergy();

//convert user input to lowercase
extern char * convert_lowercase(char *str);

//remove punctuation in user input
extern char * remove_punc(char *str);

//trim trailing white spaces in user input
extern char *trim_trailing(char *str);

//trim leading white spaces in user input
extern char * trim_leading(char *str);

//check if keyword exists in chatbot knowledge,
//if exists, return value as sign
bool search_key(char *word,char *str);

//check if reply exists in chatbot knowledge,
//if exists, return value as sign
bool match_reply(char **reply,char *str);

//function that write user input into a text file (chat.txt)
int write_chat(char *str);

//function that search in text file if the user
//input the same sentense as previous input and print a message to notify
int search_file(char *str);

//function that replace the user input with a substring
//to make the chatbot look intelligent 
void replace_substring(char *str);


#endif
