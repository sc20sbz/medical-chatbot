#include "unity.h"
#include "knowledge.h"


void  test_trim_trailing() {
  
  char input[100] = "I have covid       "; 
  char inputtwo[100] = " I!!have@@Covid       ";
  char inputthree[100] = "      I have covid_      ";
  
  TEST_ASSERT_EQUAL_STRING("I have covid", trim_trailing(input));
  TEST_ASSERT_EQUAL_STRING(" I!!have@@Covid",trim_trailing(inputtwo));
  TEST_ASSERT_EQUAL_STRING("      I have covid_", trim_trailing(inputthree));

}


void  test_convert_lowercase() {
  
  char input[100] = "I HAVE FLU"; 
  char inputtwo[100] = " I123 HAVE FLU12";
  char inputthree[100] = " I @HAVE@ FLU!";
  
  TEST_ASSERT_EQUAL_STRING("i have flu", convert_lowercase(input));
  TEST_ASSERT_EQUAL_STRING(" i123 have flu12", convert_lowercase(inputtwo));
  TEST_ASSERT_EQUAL_STRING(" i @have@ flu!", convert_lowercase(inputthree));

}


void  test_remove_punc() {
  
  char input[100] = "Shiver!!!!ing"; 
  char inputtwo[100] = " !Cou2gh??";
  char inputthree[100] = " !11?2??";
   
  TEST_ASSERT_EQUAL_STRING("Shivering", remove_punc(input));
  TEST_ASSERT_EQUAL_STRING("Cough", remove_punc(inputtwo));
   TEST_ASSERT_EQUAL_STRING("", remove_punc(inputthree));
}

void  test_trim_leading() {
  
  char input[100] = "         I am sick"; 
  char inputtwo[100] = "           !I11?2sick??";
  char inputthree[100] = "           !Iam11?2sick              ";
  
  TEST_ASSERT_EQUAL_STRING("I am sick", trim_leading(input));
  TEST_ASSERT_EQUAL_STRING("!I11?2sick??", trim_leading(inputtwo));
  TEST_ASSERT_EQUAL_STRING("!Iam11?2sick              ",trim_leading(inputthree));
}

void test_search_key() {
    int i;
    char input[100] = "fever"; 
    char inputtwo[100] = "fever";
    i = search_key(input,inputtwo);
    TEST_ASSERT_TRUE(i==1);
}

void test_match_reply() {
    int i,j;
    char * input[] = {"fever"}; 
    char inputtwo[100] = " fever";
  
    i = match_reply(input,inputtwo);
    TEST_ASSERT_TRUE(i==1);
    
}



void setUp() {
	 //this function is called before each test, it can be empty
}

void tearDown() {
	 //this function is called after each test, it can be empty
}

int main() {
	UNITY_BEGIN();


	RUN_TEST(test_trim_trailing);
	RUN_TEST(test_convert_lowercase);
	RUN_TEST(test_remove_punc);
	RUN_TEST(test_trim_leading);
	RUN_TEST(test_search_key);
    RUN_TEST(test_match_reply);
	return UNITY_END();
}
